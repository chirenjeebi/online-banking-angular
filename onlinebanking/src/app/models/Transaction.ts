import { Time } from "@angular/common";

export interface Transaction {
	transactionId:number;
	transactionDateTime:String;
	creditAmount:number;
	debitAmount:number;
	description:string;

}