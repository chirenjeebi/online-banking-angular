import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';

import { AccountCreateComponent } from './account-create.component';

describe('AccountCreateComponent', () => {
  let component: AccountCreateComponent;
  let fixture: ComponentFixture<AccountCreateComponent>;
  let service: UserService;
  let fb: FormBuilder;
  let serviceSpy;
  let toastrService;
  let httpMock: HttpTestingController;
TestBed.configureTestingModule({
      declarations: [ AccountCreateComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      providers: [{provide: UserService, useClass: serviceSpy}]
    })

  const dummyTokenData = {
    clientId: 3,
    username: "starjohnson",
    password: "password",
    firstName: "Star",
    lastName: "Johnson",
    address: "Space",
    age: 55,
    email: "starjohnson@maildrop.cc",
    contactNumber: "5015558888",
    photoUrl: null,
    accList: [
        {
            accountNumber: "132548101",
            balance: 5005.02,
            transList: [
                {
                    transactionId: 4,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 0.0,
                    creditAmount: 5000.02,
                    currentBalance: 5005.02,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 1,
                typeName: "checking"
            },
            bholder: {
                branchId: 1,
                branchName: "Santa Clara"
            }
        },
        {
            accountNumber: "132548102",
            balance: 4.999519998E7,
            transList: [
                {
                   transactionId: 1,
                   description: "Payday",
                   TransactionDateTime: "2021-01-28, 23:41",
                   debitAmount: 0.0,
                   creditAmount: 200.0,
                   currentBalance: 5.00002E7,
                   transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 2,
                    description: "Purchase of Amazing Figure",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 280.0,
                    creditAmount: 0.0,
                    currentBalance: 4.999992E7,
                    transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 3,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 5000.02,
                    creditAmount: 0.0,
                    currentBalance: 4.999519998E7,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 2,
                typeName: "savings"
            },
            bholder: {
                branchId: 3,
                branchName: "San Francisco"
            }
        }
    ]
};

  const dummyData = {
    accountNumber: "132548102"
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCreateComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, ToastrModule.forRoot(), RouterTestingModule.withRoutes([])],
      providers: [UserService,ToastrService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccountCreateComponent);
    service = TestBed.inject(UserService);
    toastrService = TestBed.inject(ToastrService);
    localStorage.setItem('token', JSON.stringify(dummyTokenData));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call createNewAccount function ', ()=>{
    serviceSpy = spyOn(service, `createNewAccount`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(serviceSpy).toHaveBeenCalled();

  });
});

