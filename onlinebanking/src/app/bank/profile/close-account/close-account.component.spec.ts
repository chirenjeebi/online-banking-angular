import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {  ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';

import { CloseAccountComponent } from './close-account.component';

describe('CloseAccountComponent', () => {
  let component: CloseAccountComponent;
  let fixture: ComponentFixture<CloseAccountComponent>;
  let httpmock: HttpTestingController;
  let serviceSpy;
  let toastrService : ToastrService;
  let router : Router;
  let service: UserService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloseAccountComponent ],
      imports: [ReactiveFormsModule, HttpClientTestingModule,ToastrModule.forRoot(),RouterTestingModule.withRoutes([])],
      providers: [ToastrService, UserService]
     
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseAccountComponent);
    component = fixture.componentInstance;
    toastrService = TestBed.inject(ToastrService);
    service = TestBed.inject(UserService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call submit request', () =>{
    serviceSpy = spyOn(service, `deleteAccount`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(serviceSpy).toHaveBeenCalled();
  });
});
