import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DepositserviceService {
_url ='http://localhost:9030/api/transaction/deposit';

formModel = this.fb.group({
  account_number:[''],
  creditAmount:[''],
  description:[''],
 

})
constructor(private fb:FormBuilder, private http:HttpClient) { }

  depositing(accNum,creditAmount,description){

    // console.log(this.formModel);
var body = {
  account_number:accNum,
  creditAmount:creditAmount,
  description:description
}
// console.log(body);
    return this.http.post<any>(this._url, body);
}
}
