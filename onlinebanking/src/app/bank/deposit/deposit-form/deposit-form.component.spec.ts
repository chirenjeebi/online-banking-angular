import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepositserviceService } from '../depositservice.service';

import { DepositFormComponent } from './deposit-form.component';

describe('DepositFormComponent', () => {
  let component: DepositFormComponent;
  let fixture: ComponentFixture<DepositFormComponent>;
  let depserv: DepositserviceService;
  let fb : FormBuilder;
  let httpmock: HttpTestingController;
  let serviceSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositFormComponent ],
      imports: [ReactiveFormsModule, HttpClientTestingModule,FormsModule],
      providers : [DepositserviceService]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositFormComponent);
    component = fixture.componentInstance;
    depserv = TestBed.inject(DepositserviceService);
    fixture.detectChanges();
  });
  const dummyTokenData = {
    clientId: 3,
    username: "starjohnson",
    password: "password",
    firstName: "Star",
    lastName: "Johnson",
    address: "Space",
    age: 55,
    email: "starjohnson@maildrop.cc",
    contactNumber: "5015558888",
    photoUrl: null,
    accList: [
        {
            accountNumber: "132548101",
            balance: 5005.02,
            transList: [
                {
                    transactionId: 4,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 0.0,
                    creditAmount: 5000.02,
                    currentBalance: 5005.02,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 1,
                typeName: "checking"
            },
            bholder: {
                branchId: 1,
                branchName: "Santa Clara"
            }
        },
        {
            accountNumber: "132548102",
            balance: 4.999519998E7,
            transList: [
                {
                   transactionId: 1,
                   description: "Payday",
                   TransactionDateTime: "2021-01-28, 23:41",
                   debitAmount: 0.0,
                   creditAmount: 200.0,
                   currentBalance: 5.00002E7,
                   transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 2,
                    description: "Purchase of Amazing Figure",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 280.0,
                    creditAmount: 0.0,
                    currentBalance: 4.999992E7,
                    transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 3,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 5000.02,
                    creditAmount: 0.0,
                    currentBalance: 4.999519998E7,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 2,
                typeName: "savings"
            },
            bholder: {
                branchId: 3,
                branchName: "San Francisco"
            }
        }
    ]
};

  const dummyData = {
    creditAmount: ".02",
    description : "payday"

  }
 /*  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FundTransferComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      providers: [UserService]
    })
    .compileComponents();
 */
    /* fixture = TestBed.createComponent(FundTransferComponent);

    service = TestBed.inject(UserService);
    localStorage.setItem('token', JSON.stringify(dummyTokenData));
    component = fixture.componentInstance;
    fixture.detectChanges();
  }); */

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onSubmit()', () =>{
    fixture.componentInstance.formModel.creditAmount= dummyData.creditAmount;
    // const a1 = fixture.componentInstance.formModel.amount;
    // console.log(a1);
    serviceSpy = spyOn(depserv, `depositing`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(serviceSpy).toHaveBeenCalled();
  });
});
