import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class withdrawservice {
_url ='http://localhost:9030/api/transaction/withdraw';

formModel = this.fb.group({
  account_number:[''],
  debitAmount:[''],
  description:[''],
 

})
constructor(private fb:FormBuilder, private http:HttpClient) { }

  withdrawing(accNum,debitAmount,description){

    // console.log(this.formModel);
var body = {
  account_number:accNum,
  debitAmount:debitAmount,
  description:description
}
// console.log(body);
      return this.http.post<any>(this._url, body);
  }
}
