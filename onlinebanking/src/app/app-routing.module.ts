import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { BalanceComponent } from './bank/balance/balance.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { FundTransferComponent } from './bank/fund-transfer/fund-transfer.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './bank/profile/profile.component';
import { StatementComponent } from './bank/statement/statement.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserComponent } from './user/user.component';
import { WithdrawComponent } from './bank/withdraw/withdraw.component';
import { BankComponent } from './bank/bank.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfileUpdateComponent } from './bank/profile/profile-update/profile-update.component';
import { AccountCreateComponent } from './bank/profile/account-create/account-create.component';
import { ViewProfileComponent } from './bank/profile/view-profile/view-profile.component';
import { CloseAccountComponent } from './bank/profile/close-account/close-account.component';
import { ForgotPasswordComponent } from './user/forgot-password/forgot-password.component';
import { DepositFormComponent } from './bank/deposit/deposit-form/deposit-form.component';

const routes: Routes = [
  {path:'', redirectTo:'/user/login', pathMatch:'full'},
  {
    path:'user', component:UserComponent,
    children:[
      {path:'login', component:LoginComponent},
      {path:'register', component:RegistrationComponent},
      {path: 'forgotpassword', component:ForgotPasswordComponent},
      {path:"#", component:LoginComponent}
    ]
  },
  {path:'home', component:HomeComponent},
  {path:'forbidden', component:ForbiddenComponent},
  {path:'adminpanel', component:AdminPanelComponent},
  {
     path:'bank', component:BankComponent,  canActivate:[AuthGuard],
  //  path:'bank', component:BankComponent, 
    children:[
      {path:'profile', component:ProfileComponent,
        children:[
        {path:'updateprofile', component:ProfileUpdateComponent},
        {path:'createaccount', component:AccountCreateComponent},
        {path:'viewprofile', component:ViewProfileComponent},
        {path:'closeaccount', component:CloseAccountComponent}
      ]
      },
      {path:'deposit', component:DepositFormComponent},
      {path:'withdraw', component:WithdrawComponent},
      {path:'transfer', component:FundTransferComponent},
      {path:'balance', component: BalanceComponent},
      {path:'statement', component:StatementComponent},
      {path:"#", component:LoginComponent}
    ],
  },
   {path:"#", component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ProfileComponent, DepositFormComponent, WithdrawComponent, FundTransferComponent, BalanceComponent, StatementComponent];
export const userRouting = [LoginComponent, RegistrationComponent];
