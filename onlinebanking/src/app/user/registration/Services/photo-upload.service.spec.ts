import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule } from '@angular/forms';

import { PhotoUploadService } from './photo-upload.service';

describe('PhotoUploadService', () => {
  let service: PhotoUploadService;
  let httpMock : HttpTestingController;
  let file : File;
  let fb : FormBuilder
  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [HttpClientTestingModule, FormsModule],
      providers: [PhotoUploadService],
    });
    service = TestBed.inject(PhotoUploadService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call pushFileToStorage', ()=>{
    service.pushFileToStorage(file).subscribe((res) => {
      expect(res.toString()).toEqual(JSON.stringify(file));
    })

    const req = httpMock.expectOne('http://localhost:9030/uploadFile');
    expect(req.request.method).toBe('POST');
  })
});
