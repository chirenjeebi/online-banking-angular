import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PhotoUploadService {
  request: HttpRequest<FormData>;
  data: FormData;
  constructor(private https: HttpClient) { }
  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    this.data = new FormData();
    this.data.append('file', file);
    const newRequest = new HttpRequest('POST', 'http://localhost:9030/uploadFile', this.data, {
      reportProgress: true,
      responseType: 'text'
    });
    this.request = newRequest;
    return this.https.request(newRequest);
  }

}
