import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { By } from '@angular/platform-browser'; 
import { UserService } from 'src/app/shared/user.service';
import { LoginComponent } from './login.component';
import { Observable } from 'rxjs';
import { BankComponent } from 'src/app/bank/bank.component';
import { Location } from '@angular/common';

//const routerSpy = jasmine.createSpyObj('Router', ['navigateByURL']);

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;
  let toastrService: ToastrService;
  let service: UserService
  let httpMock : HttpTestingController;
  let fb : FormBuilder;
 
  let loginSpy;
  let routerSpy;
  
  function updateForm(username, password){
    fixture.componentInstance.formModel.username = username;
    fixture.componentInstance.formModel.password = password;
     
  }
  
     TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([{path: './bank', component: BankComponent}]), ToastrModule.forRoot()],
      providers: [
        {provide:UserService, useValue: loginSpy},
        {provide: Router, userValue: router}, ToastrService]
    })
    .compileComponents();
  const dummyTokenData = {
      clientId: 3,
      username: "starjohnson",
      password: "password",
      firstName: "Star",
      lastName: "Johnson",
      address: "Space",
      age: 55,
      email: "starjohnson@maildrop.cc",
      contactNumber: "5015558888",
      photoUrl: null,
      accList: [
          {
              accountNumber: "132548101",
              balance: 5005.02,
              transList: [
                  {
                      transactionId: 4,
                      description: "Transfer of money from savings to checking",
                      TransactionDateTime: "2021-01-28, 23:41",
                      debitAmount: 0.0,
                      creditAmount: 5000.02,
                      currentBalance: 5005.02,
                      transactionDateTime: "2021-01-28, 23:41"
                  }
              ],
              tholder: {
                  typeId: 1,
                  typeName: "checking"
              },
              bholder: {
                  branchId: 1,
                  branchName: "Santa Clara"
              }
          },
          {
              accountNumber: "132548102",
              balance: 4.999519998E7,
              transList: [
                  {
                     transactionId: 1,
                     description: "Payday",
                     TransactionDateTime: "2021-01-28, 23:41",
                     debitAmount: 0.0,
                     creditAmount: 200.0,
                     currentBalance: 5.00002E7,
                     transactionDateTime: "2021-01-28, 23:41"
                  },
                  {
                      transactionId: 2,
                      description: "Purchase of Amazing Figure",
                      TransactionDateTime: "2021-01-28, 23:41",
                      debitAmount: 280.0,
                      creditAmount: 0.0,
                      currentBalance: 4.999992E7,
                      transactionDateTime: "2021-01-28, 23:41"
                  },
                  {
                      transactionId: 3,
                      description: "Transfer of money from savings to checking",
                      TransactionDateTime: "2021-01-28, 23:41",
                      debitAmount: 5000.02,
                      creditAmount: 0.0,
                      currentBalance: 4.999519998E7,
                      transactionDateTime: "2021-01-28, 23:41"
                  }
              ],
              tholder: {
                  typeId: 2,
                  typeName: "savings"
              },
              bholder: {
                  branchId: 3,
                  branchName: "San Francisco"
              }
          }
      ]
  };


    const dummyLoginData = {
      username: 'starjohnson',
      password: 'password'
    };
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([]), ToastrModule.forRoot()],
      providers: [UserService, ToastrService]
    })
    .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    
    fixture.detectChanges();

    router = TestBed.inject(Router);
    service = TestBed.inject(UserService);
    toastrService = TestBed.inject(ToastrService);
  });

  it('should have been created', () => {
    expect(component).toBeTruthy();
  });
  it('should be able to call login', () =>{
    updateForm(dummyLoginData.username, dummyLoginData.password);
    loginSpy = spyOn(service, 'login').and.returnValue(Observable.create(observer=>{
      observer.next(dummyTokenData);
    }))
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(loginSpy).toHaveBeenCalled();
      });
      it('should route to bank if login successfully', () =>{
        updateForm(dummyLoginData.username, dummyLoginData.password);
        const button = fixture.debugElement.nativeElement.querySelector('button');
        button.click();
        fixture.detectChanges();
        loginSpy = spyOn(service, 'login').and.returnValue(Observable.create(observer=>{
          observer.next(dummyTokenData);
        }))
        fixture.detectChanges();

        router = TestBed.get(Router);
        let location:Location = TestBed.get(Location);
        router.navigate(['./bank']).then(()=>{
          expect(location.path()).toBe('./bank');
          console.log(location.path());
        });

        // routerSpy = spyOn(router, 'navigate');
        // expect(routerSpy.navigate).toHaveBeenCalled();
        // const navArgs = routerSpy.navigateByURL('/bank');
        // expect(navArgs).toBe('/bank', 'should navigate to bank home');
      });
});
